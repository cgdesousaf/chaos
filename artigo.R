library(parallel)
require(fftw)
require(ggplot2)
require(ggthemes)
# require(ggpubr)
require(reshape2)
require(ggrepel)
source("bandt_pompe/bandt_pompe.R")
source("bandt_pompe/measures.R")
source("bandt_pompe/features.R")
source("bandt_pompe/visibility.R")
source("bandt_pompe/helpers.R")
bp_path="./bandt_pompe/"

# setwd("c:/Users/ramos/Documents/Marcelo/")
# setwd("/Users/cristopher/Workspace/chaos/")

random_files <- list.files("random_numbers/")

#################Fun??es#########################################################
main = function(i){
  print(i)
  n = config$TN[i]
  id = config$REP[i]
  dados = unlist(read.table(paste("random_numbers/", random_files[id], sep = ""), header = TRUE, nrows = n))
  dados = knoise(dados,config$K[i])
  names(dados) = NULL
  temp = complexity_entropy(dados,config$D[i],config$TAU[i])
  return(c(config[i,],temp))
}

knoise = function(x, k){
  
  n = length(x)
  x = x - mean(x)
  p <- planFFT(n)
  y <- FFT(x, plan=p)
  
  Series <- Spectrum <- vector(mode="numeric")
  Power <- vector(mode="character")
  
  filtro <- (1:n)^-(k/2)
  filtro <- filtro / sum(filtro)
  y1 <- y * filtro     # Spectrum smoothing
  x1 <- IFFT(y1, plan=p)  # Back to time domain
  
  Series <- c(Series, Re(x1))
  #Spectrum <- c(Spectrum, Mod(y1))
  #Power <- c(Power, rep(k, n))  
  
  #Series.and.PowerSpectra <- data.frame(Series, Spectrum, Power=as.factor(Power))
  return(Series)
}


##################################################################################

####################inicializa??o de vari?veis####################################

TN=c(10^3, 10^4, 5*10^4, 10^5, 5*10^5) #tamanho da sequ?ncia
K=c(0, 0.1, 0.5, 1, 1.5, 2) #cor do ru?do
D = c(3,4,5,6) #tamanho da palavra BP
REP=1:1000 #n?mero de replica??es de cada cen?rio
TAU=1
config = expand.grid(TN,K,D,REP,TAU) #config de cada replica??o
names(config) = c("TN","K","D","REP","TAU")
nit = nrow(config)

#################### Estimando tempo de execução ###################

# eta <- c()
# 
# for( n in TN ){
#   eta <- rbind(eta, system.time(dados <- unlist(read.table(paste("random_numbers/", random_files[1], sep = ""), header = TRUE, nrows = n))))
#   eta <- rbind(eta, system.time(temp <- complexity_entropy(dados,3,1))) 
# }
# 
# final_eta <- sum(eta[,3]) * length(K) * length(D) * max(REP) # Análise de todos os cenários
# 
# final_eta <- final_eta / 3600 # Convertendo para horas
# 
# print("Tempo de execução estimado em horas: ")
# print(final_eta)

#######################configurando paralelismo##################################
ncores = detectCores() - 2
cl = makeCluster(ncores, outfile = "log.txt")
clusterSetRNGStream(cl,iseed=1234567890)
clusterExport(cl,c("config","complexity_entropy",
                   "bandt_pompe_distribution","bandt_pompe","permutations",
                   "shannon_entropy","complexity","random_files", "knoise"))
clusterEvalQ(cl,library(fftw))
#################################################################################

#####################Main Loop###################################################
output = parSapply(cl, 1:nit, main)
stopCluster(cl)

names(output) = c("TN","K","D","REP","TAU","H","C","JS")
write.table(output, file = "result.txt", row.names = FALSE)
#################################################################################
