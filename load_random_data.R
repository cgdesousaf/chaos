load_random_data <- function(n){
  files <- list.files(path = "data/grouped/")
  
  data <- c()
  
  # Reading random.org data
  for(file in files){
    print(file)
    data <- append(data, readBin(paste("data/grouped/", file, sep = ""),n=1e8,size="4",what ='integer'))
  }
  
  # Reading quantic data
  data <- append(data, readBin("data/cohnopafinal_0.65_sha512_portion00",n=1e8,size="4",what ='integer'))
  
  # Removing NAs and normalizing data
  data <- na.exclude(abs(data/data[which.max(data)]))
  
  return(data)
}